import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.ionic.vue.memoryapp',
  appName: 'Memory App',
  webDir: 'dist',
  bundledWebRuntime: true,
};

export default config;
