import { createRouter, createWebHistory } from '@ionic/vue-router';
import { RouteRecordRaw } from 'vue-router';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    redirect: '/memories',
  },
  {
    path: '/memories',
    component: () => import('../pages/MemoriesPage.vue'),
  },
  {
    path: '/memories/add',
    component: () => import('../pages/AddMemoryPage.vue'),
  },
  {
    path: '/memories/:id',
    component: () => import('../pages/MemoryDetailsPage.vue'),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
