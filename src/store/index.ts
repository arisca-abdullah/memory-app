import { InjectionKey } from 'vue';
import { createStore, Store } from 'vuex';

export interface MemoryData {
  image: string;
  title: string;
  description: string;
}

export interface Memory extends MemoryData {
  id: string;
}

export interface State {
  memories: Memory[];
}

export const key: InjectionKey<Store<State>> = Symbol();

const state: State = {
  memories: [
    {
      id: 'm1',
      image:
        'https://upload.wikimedia.org/wikipedia/commons/thumb/7/76/Mighty_Mountains_with_Snow.jpg/640px-Mighty_Mountains_with_Snow.jpg',
      title: 'A trip into the mountains',
      description: 'It was a really nice trip!',
    },
    {
      id: 'm2',
      image:
        'https://upload.wikimedia.org/wikipedia/commons/thumb/4/4c/A_surfer_surfing_on_the_ocean_%28Unsplash%29.jpg/640px-A_surfer_surfing_on_the_ocean_%28Unsplash%29.jpg',
      title: 'Surfing the sea side',
      description: 'Feeling the cool breeze',
    },
    {
      id: 'm3',
      image:
        'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a9/Indian_-_Food.jpg/640px-Indian_-_Food.jpg',
      title: 'Good eating',
      description: 'Really tasty!',
    },
  ],
};

const store = createStore<State>({
  state,
  getters: {
    memories(state: State) {
      return state.memories;
    },
    memory(state: State) {
      return (memoryId: string) =>
        state.memories.find((memory: Memory) => memory.id === memoryId);
    },
  },
  mutations: {
    addMemory(state: State, memoryData: MemoryData) {
      const newMemory: Memory = {
        id: new Date().toISOString(),
        ...memoryData,
      };

      state.memories.unshift(newMemory);
    },
  },
  actions: {
    addMemory(context, memoryData: MemoryData) {
      context.commit('addMemory', memoryData);
    },
  },
  modules: {},
});

export default store;
